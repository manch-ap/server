/**
 * Log Unhandled Exceptions
 */
process.on('uncaughtException', (err) => {
    console.log(err);
});

// Unhandled Promise Result
process.on('unhandledRejection', (err) => {
    console.log(err);
});

/**
 * Constants
 */
const PORT = process.env.PORT || 5000;

/**
 * Libs
 */
const express = require("express");
const helmet = require("helmet");
const cors = require("cors");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const errorHandler = require("./lib/middleware/error");
const userRouter = require("./lib/router/user");
const postRouter = require("./lib/router/post");
const followerRouter = require("./lib/router/follower");
const app = express();
const mongo = require("./lib/db/mongo");

/**
 * 1. Attach Pre Middleware
 */
app.use(helmet());
app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json());

/**
 * 2. Attach Routers
 */
userRouter.attach(app);
postRouter.attach(app);
followerRouter.attach(app);

/**
 * 3. Attach Post Middleware
 */
app.use(errorHandler());

/**
 * Start listening
 */
async function startService() {
    try {
        // Connect to MongoDB
        await mongo.connect();
        // Listen
        app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));
    } catch (err) {
        // Log
        console.log(err);
        // Retry after 10 seconds
        setTimeout(startService, 10000);
    }
}
// Exec
startService();
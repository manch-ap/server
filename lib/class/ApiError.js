const uuid = require("uuid/v4");

module.exports = class ApiError extends Error {
    constructor(error, status, extra) {
        if(error instanceof Error) {
            super(error.message);
            this.message = error.message;
            this.stack = error.stack;
        } else {
            if(error instanceof Object) {
                error = JSON.stringify(error);
            }
            super(error);
            this.message = error;
            Error.captureStackTrace(this, this.constructor);
        }
        this.id = uuid();
        this.name = this.constructor.name;
        this.status = status || 500;
        this.extra = extra;
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name,
            status: this.status,
            message: this.message,
            extra: this.extra,
            stack: this.stack
        };
    }
};
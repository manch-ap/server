const uuid = require("uuid/v4");
const ApiError = require("../class/ApiError");

module.exports = function() {
    return function(err, req, res, next) {
        if(!res.headersSent) {
            let error;
            if(err instanceof ApiError) {
                error = err.toJSON();
            } else {
                error = {
                    id: uuid(),
                    name: err.name,
                    status: 500,
                    message: err.message,
                    stack: err.stack
                };
            }
            if(process.env.NODE_ENV == 'production') {
                delete error.stack;
            }
            res.status(error.status).json(error);
        }
        next(err);
    };
};
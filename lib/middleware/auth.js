const jwt = require("../module/jwt");
const ApiError = require("../class/ApiError");
const { User } = require("../model/model");

module.exports = function() {
    return async function(req, res, next) {
        try {
            // Get Token from Header
            let token = req.headers["x-access-token"];
            if (!token) {
                throw new ApiError("Invalid Token", 403);
            }
            // Verify Token
            let user = await jwt.verify(token);
            // Check if user exists
            user = await User.model.findById(user.id);
            if (!user) {
                throw new ApiError("User does not exist", 403);
            }
            req.user = user.toJSON();
            // Done
            next();
        } catch (err) {
            next(err);
        }
    };
};
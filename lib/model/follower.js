const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const paginate = require("mongoose-paginate-v2");
const deepPopulate = require('mongoose-deep-populate')(mongoose);
const uuid = require("uuid/v4");

// Create Schema
const FollowerSchema = new Schema({
    // Id
    _id: {
        type: String
    },
    // User
    user: {
        type: String,
        ref: "User",
        required: true
    },
    // Follower
    follower: {
        type: String,
        ref: "User",
        required: true
    }
}, {
    timestamps: true,
    autoIndex: true,
    strict: true,
    useNestedStrict: true
});

FollowerSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    retainKeyOrder: true,
    minimize: false,
    transform: function(doc, ret) {
        delete ret._id;
    }
});

// Attach Plugins
FollowerSchema.plugin(paginate);
FollowerSchema.plugin(deepPopulate);

// Create Model
const FollowerModel = mongoose.model('Follower', FollowerSchema);

// Export
module.exports.schema = FollowerSchema;
module.exports.model = FollowerModel;
module.exports.generateId = uuid;
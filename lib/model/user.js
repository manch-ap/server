const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const paginate = require("mongoose-paginate-v2");
const deepPopulate = require('mongoose-deep-populate')(mongoose);
const uuid = require("uuid/v4");

// Create Schema
const UserSchema = new Schema({
    // Id
    _id: {
        type: String
    },
    // Name
    name: {
        type: String,
        trim: true,
        minlength: 1,
        maxlength: 100,
        required: true,
        index: true
    },
    // Email
    email: {
        type: String,
        required: true,
        validate: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
        index: {
            unique: true,
            dropDups: true
        }
    },
    // Hashed Password
    password: {
        type: String,
        required: true
    }
}, {
    timestamps: true,
    autoIndex: true,
    strict: true,
    useNestedStrict: true
});

UserSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    retainKeyOrder: true,
    minimize: false,
    transform: function(doc, ret) {
        delete ret._id;
        delete ret.password;
        delete ret.createdAt;
        delete ret.updatedAt;
    }
});

UserSchema.index({
    name: "text",
    email: "text"
});

// Attach Plugins
UserSchema.plugin(paginate);
UserSchema.plugin(deepPopulate);

// Create Model
const UserModel = mongoose.model('User', UserSchema);

// Export
module.exports.schema = UserSchema;
module.exports.model = UserModel;
module.exports.generateId = uuid;
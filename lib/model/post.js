const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const paginate = require("mongoose-paginate-v2");
const deepPopulate = require('mongoose-deep-populate')(mongoose);
const uuid = require("uuid/v4");

// Create Schema
const PostSchema = new Schema({
    // Id
    _id: {
        type: String
    },
    // Post Owner
    user: {
        type: String,
        ref: "User",
        required: true
    },
    // Text
    text: {
        type: String,
        minlength: 1,
        maxlength: 10000,
        required: true
    }
}, {
    timestamps: true,
    autoIndex: true,
    strict: true,
    useNestedStrict: true
});

PostSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    retainKeyOrder: true,
    minimize: false,
    transform: function(doc, ret) {
        delete ret._id;
    }
});

// Attach Plugins
PostSchema.plugin(paginate);
PostSchema.plugin(deepPopulate);

// Create Model
const PostModel = mongoose.model('Post', PostSchema);

// Export
module.exports.schema = PostSchema;
module.exports.model = PostModel;
module.exports.generateId = uuid;
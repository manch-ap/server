const SECRET = process.env.JWT_SECRET;
const jwt = require("jsonwebtoken");

module.exports.sign = function(payload) {
    return new Promise(function(resolve, reject) {
        jwt.sign(payload, SECRET, {
            expiresIn: "7d"
        }, (err, token) => {
            if (err) {
                reject(err);
            } else {
                resolve(token);
            }
        });
    });
};

module.exports.verify = function(token) {
    return new Promise(function(resolve, reject) {
        jwt.verify(token, SECRET, {}, (err, decoded) => {
            if (err) {
                reject(err);
            } else {
                resolve(decoded);
            }
        });
    });
};
const ApiError = require("../class/ApiError");
const SecPass = require('secure-password');
const hasher = SecPass({
    memlimit: SecPass.MEMLIMIT_DEFAULT,
    opslimit: SecPass.OPSLIMIT_DEFAULT
});

// Hash
const hash = function(data) {
    return new Promise((resolve, reject) => {
        hasher.hash(Buffer.from(data), (err, hash) => {
            if(err) {
                reject(err);
            } else {
                resolve(hash);
            }
        });
    });
};

// Verify
const verify = function(data, hash) {
    return new Promise((resolve, reject) => {
        hasher.verify(Buffer.from(data), Buffer.from(hash), async (err, res) => {
            if(err) {
                reject(new ApiError(err, 401));
            } else {
                switch(res) {
                    case SecPass.VALID:
                        resolve();
                        break;
                    case SecPass.VALID_NEEDS_REHASH:
                        try {
                            resolve(await hash(data));
                        } catch (err) {
                            resolve();
                        }
                        break;
                    default:
                        reject(new ApiError("Verification failed", 401));
                }
            }
        });
    });
};

module.exports.hash = hash;
module.exports.verify = verify;

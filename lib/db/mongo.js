const mongoose = require("mongoose");

module.exports.connect = function () {
    return new Promise(async function(resolve, reject) {
        try {
            console.log("Setting up DB...");
            // Create Options
            const options = {
                useNewUrlParser: true,
                useCreateIndex: true,
                useFindAndModify: false,
                socketTimeoutMS: 0,
                keepAlive: true,
                autoReconnect: true,
                reconnectTries: Infinity,
                reconnectInterval: 5000,
                poolSize: 5
            };
            // Get Connection Object
            const connection = mongoose.connection;
            // Attach Listeners
            connection.on('error', (err) => {
                if(err) {
                    console.log(err);
                }
            });
            connection.on('open', () => console.log("Connected to MongoDB"));
            // Connect to DB
            await mongoose.connect(process.env.MONGODB_URI, options);
            // Done
            resolve();
        } catch (err) {
            reject(err);
        }
    });
};
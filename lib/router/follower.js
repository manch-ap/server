const ApiError = require("../class/ApiError");
const { Router } = require("express");
const { User, Follower } = require("../model/model");
const authMW = require("../middleware/auth");

// Instantiate Router
let router = new Router();

// Setup to use Auth Middleware
router.use(authMW());

// Create Endpoints
// 1. Follow User
router.put("/follow/:id", async (req, res, next) => {
    try {
        // Validate Data
        let { id: userid } = req.params;
        if (!userid) {
            throw new ApiError("User Id cannot be empty", 400);
        }
        // Check if User exists and that target user is not current user
        let user = await User.model.findById(userid);
        if (!user) {
            throw new ApiError("User not found", 404);
        } else if (user.id.localeCompare(req.user.id) == 0) {
            throw new ApiError("You cannot follow yourself :P", 400);
        }
        // Check if already following
        let follower = await Follower.model.findOne({
            user: userid,
            follower: req.user.id
        });
        if (follower) {
            throw new ApiError("You are already following this user", 400);
        }
        // All Good
        // 1. Create Object
        follower = {
            id: Follower.generateId(),
            user: user.id,
            follower: req.user.id
        };
        // 2. Save
        follower = await Follower.model.findByIdAndUpdate(follower.id, follower, {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true
        });
        // Done
        res.status(200).json(follower);
    } catch (err) {
        next(err);
    }
});

// 2. Un-Follow
router.delete("/unfollow/:id", async (req, res, next) => {
    try {
        // Validate Data
        let { id: userid } = req.params;
        if (!userid) {
            throw new ApiError("User Id cannot be empty", 400);
        }
        // Check if already following
        let follower = await Follower.model.findOne({
            user: userid,
            follower: req.user.id
        });
        if (!follower) {
            throw new ApiError("You were not following this user", 400);
        }
        // Delete
        await follower.remove();
        // Done
        res.status(200).json(follower);
    } catch (err) {
        next(err);
    }
});

// 3. Get Following
router.get("/list", async (req, res, next) => {
    try {
        // Validate Data
        let { page = 1, sort = -1, size = 10 } = req.query;
        // Fetch
        let followers = await Follower.model.paginate({
            user: req.user.id
        }, {
            page: page,
            limit: size,
            populate: ["user", "follower"],
            sort: {
                createdAt: sort
            },
            customLabels: {
                docs: 'followers',
                totalDocs: 'total',
                limit: 'limit',
                page: 'current',
                nextPage: 'next',
                prevPage: 'prev',
                totalPages: 'pages',
                pagingCounter: 'pagingCounter'
            }
        });
        // Done
        res.status(200).json(followers);
    } catch (err) {
        next(err);
    }
});

// 4. Is Following
router.get("/following/:id", async (req, res, next) => {
    try {
        // Validate Data
        let { id } = req.params;
        // Check if already following
        let follower = await Follower.model.findOne({
            user: id,
            follower: req.user.id
        });
        // Done
        res.status(200).json({
            status: follower != null,
            data: follower
        });
    } catch (err) {
        next(err);
    }
});

/**
 * Export
 */
module.exports.attach = (app) => {
    app.use("/follower", router);
};
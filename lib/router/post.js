const ApiError = require("../class/ApiError");
const { Router } = require("express");
const { Post, Follower } = require("../model/model");
const authMW = require("../middleware/auth");

// Instantiate Router
let router = new Router();

// Setup to use Auth Middleware
router.use(authMW());

// Utility
async function packPost(user, post) {
    // Get Post
    post = post.toJSON ? post.toJSON() : post;
    // Check if Following
    // Get Record
    let follower = await Follower.model.findOne({
        user: post.user.id,
        follower: user.id
    });
    // Set
    post.user.self = post.user.id.localeCompare(user.id) == 0;
    post.user.following = {
        status: follower != null,
        data: follower
    };
    // Done
    return post;
}

// Create Endpoints
// 1. Create Post
router.put("", async (req, res, next) => {
    try {
        // Validate Data
        let { text } = req.body;
        if (!text) {
            throw new ApiError("Post Text cannot be empty", 400);
        }
        // 1. Create Object
        let post = {
            id: Post.generateId(),
            user: req.user.id,
            text: text
        };
        // 2. Save
        post = await Post.model.findByIdAndUpdate(post.id, post, {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true
        });
        // Populate User
        post = post.toJSON();
        post.user = req.user;
        // Done
        res.status(200).json(await packPost(req.user, post));
    } catch (err) {
        next(err);
    }
});

// 2. Get Posts
router.get("/timeline", async (req, res, next) => {
    try {
        // Validate Data
        let { mode = 0, user = null, page = 1, sort = -1, size = 10 } = req.query;
        // Vars
        let postsData;
        // Process Based on Mode
        switch (Number.parseInt(mode)) {
            // Global Timeline
            case 0:
                // Fetch
                postsData = await Post.model.paginate({}, {
                    page: page,
                    limit: size,
                    populate: ["user"],
                    sort: {
                        createdAt: sort
                    },
                    customLabels: {
                        docs: 'posts',
                        totalDocs: 'total',
                        limit: 'limit',
                        page: 'current',
                        nextPage: 'next',
                        prevPage: 'prev',
                        totalPages: 'pages',
                        pagingCounter: 'pagingCounter'
                    }
                });
                break;
            // User Timeline
            default:
                // Fetch
                postsData = await Post.model.paginate({
                    user: user
                }, {
                    page: page,
                    limit: size,
                    populate: ["user"],
                    sort: {
                        createdAt: sort
                    },
                    customLabels: {
                        docs: 'posts',
                        totalDocs: 'total',
                        limit: 'limit',
                        page: 'current',
                        nextPage: 'next',
                        prevPage: 'prev',
                        totalPages: 'pages',
                        pagingCounter: 'pagingCounter'
                    }
                });
        }
        // Add Following Data
        for (let i=0; i<postsData.posts.length; ++i) {
            postsData.posts[i] = await packPost(req.user, postsData.posts[i]);
        }
        // Done
        res.status(200).json(postsData);
    } catch (err) {
        next(err);
    }
});

// 3. Delete Post
router.delete("/:id", async function(req, res, next) {
    try {
        // Get  Post
        let post = await Post.model.findById(req.params.id);
        if (!post) {
            throw new ApiError("Post not found", 404);
        }
        // Check if current user is the post creator
        if (req.user.id.localeCompare(post.user) != 0) {
            throw new ApiError("Only the post's creator can delete his/her post", 403);
        }
        // Delete Post
        await post.remove();
        // Done
        res.status(200).json({
            message: "Post Deleted"
        });
    } catch (err) {
        next(err);
    }
});

/**
 * Export
 */
module.exports.attach = (app) => {
    app.use("/post", router);
};
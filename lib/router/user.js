const ApiError = require("../class/ApiError");
const { Router } = require("express");
const { User } = require("../model/model");
const hasher = require("../module/password.hash");
const jwt = require("../module/jwt");

// Instantiate Router
const router = new Router();

// Setup Endpoints
// 1. Signup
router.put("/signup", async (req, res, next) => {
    try {
        // Validate Data
        let { name, email, password } = req.body;
        if (!name || !email || !password) {
            throw new ApiError("Missing parameters Name or Email or Password", 400);
        }
        // Check if email is already in use
        // 1. Get User
        let user = await User.model.findOne({
            email: email
        });
        if (user) {
            throw new ApiError("Email is already in use by another account", 400);
        }
        // 2. Create Object
        user = {
            id: User.generateId(),
            name: name,
            email: email,
            password: await hasher.hash(password)
        };
        // 3. Save User
        user = await User.model.findByIdAndUpdate(user.id, user, {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true
        });
        // Done
        res.status(200).json(user);
    } catch (err) {
        next(err);
    }
});

// 2. Login
router.post("/login", async (req, res, next) => {
    try {
        // Validate Data
        let { email, password } = req.body;
        if (!email || !password) {
            throw new ApiError("Missing parameters Email or Password", 400);
        }
        // Check
        // 1. Get User
        let user = await User.model.findOne({
            email: email
        });
        if (!user) {
            throw new ApiError("User not found", 404);
        }
        // 2. Check Password
        await hasher.verify(password, user.password);
        // 3. Generate Token
        let token = await jwt.sign(user.toJSON());
        // Done
        res.set("x-access-token", token).status(200).json({
            ...user.toJSON(),
            token: token
        });
    } catch (err) {
        next(err);
    }
});

/**
 * Export
 */
module.exports.attach = (app) => {
    app.use("/user", router);
};
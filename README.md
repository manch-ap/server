# Manch AP
Manch Assignment (Node.js Server)

## Android Client
[Repo](https://gitlab.com/manch-ap/android)

## Tech Used
    1. Server: Node.js
    2. DB: MongoDB

## Dev-Ops
Hosted on Heroku

## Project setup
```npm install```

## Environment Variables Required
    1. PORT (Port to run on)
    2. MONGODB_URL (MongoDB URL)
    3. JWT_SECRET (JSON Web Token Signing Key)

## Start Server
```npm start```

## Features (API)
    1. User (User Management)
        1. Signup
        2. Login
    2. Post (Posts Management)
        1. Create
        2. Timeline
            1. Global
            2. User
        3. Delete
    3. Follower (Follower API)
        1. Follow
        2. Unfollow
        3. Following Status
        4. Followers List

### Notes
    1. This is a pretty basic implementation
    2. No Input Validation (Can use Joi library)
    3. No Caching (Emphasis on Basic Implementation)
    4. It can be scaled pretty well